#' Illustrate time trends of epidemiological data
#'
#' @param x tidyfied data frame with Nordpred observations and predictions
#' @param what Character of length one: "inc" (incidence), "prev" (prevalence)
#'   or "behandling"
#' @param diagnos Character of length one: name of diagnos to be plotted
#' @param region Character of length one: name of reigon
#' @param ages Character of length one: "all" (all ages combined) or
#'   "byage" (separate age groups)
#' @param .save either NULL (don't save but return ggplot object) or character
#' vecktor defining formats to save (for example c(".png", ".eps"))
#'
#' @return ggplot object if .save = NULL or NULL (with side effect)
ggepi <- function(x, what, diagnos = "", region = "", ages, caption = "", .save = NULL) {


  # RCC profile colors
  legend_linetype <- c(
    "Observerat antal"                 = "solid",
    "Befolkningstrend + incidenstrend" = "dashed",
    "Befolkningstrend"                 = "dotted"
  )

  subtitle <-
    switch(what,
      inc  = paste("Incidens i",  region),
      prev = paste("Prevalens i", region),
      ""
    )

  df <-
    mutate(x,
      predmethod = factor(predmethod,
        c("pop", "popinc"),
        c("Befolkningstrend", "Befolkningstrend + incidenstrend")),
      predmethod =
        if_else(observed, "Observerat antal", as.character(predmethod)
    ))

  # One prediction method is enough for prev
  if (what == "prev") {
    df <- filter(df, predmethod != "Befolkningstrend")
    legend_linetype <- legend_linetype[-3]
  }

  # Adjust size
  theme_size       <- if (ages == "byage") 5 else 10
  annotation_size  <- if (ages == "byage") 1.5 else 2
  point_size       <- if (ages == "byage") .75 else 2

  p <-
    ggplot(df,
    aes(period, nn, group = 1, label = format(nn, big.mark = " "))) +
    geom_line(data = filter(df, observed),
      show.legend = FALSE, color = rcc_colors[1]) +
    geom_line(
      aes(linetype = "Befolkningstrend + incidenstrend"),
      data = filter(df,
        period == "2011-2015" | predmethod == "Befolkningstrend + incidenstrend"
      ),
      color = rcc_colors[1]
    ) +
    geom_line(
      aes(linetype = "Befolkningstrend"),
      data = filter(df,
        period == "2011-2015" | predmethod == "Befolkningstrend"),
      show.legend = FALSE,
      color = rcc_colors[1]
    ) +
    theme_minimal(theme_size) +
    scale_linetype_manual(
      "nn",
      values = legend_linetype,
      breaks = names(legend_linetype)
    ) +
    scale_y_continuous(
      labels = scales::format_format(big.mark = " ")
    ) +
    # expand_limits(y = 0) +
    guides(linetype = guide_legend(title = "")) +
    theme(
      legend.position      = "bottom",
      axis.text.x          = element_text(angle = 30),
      axis.title.x         = element_blank()
    ) +
    geom_point(show.legend = FALSE, color = rcc_colors[1], size = point_size) +
    geom_point(
      data = filter(df, !observed),
      size = 2 * annotation_size, color = I("white"), show.legend = FALSE
    ) +
    geom_text(
      data  = filter(df, !observed),
      color = rcc_colors[2],
      size  = annotation_size,
      show.legend = FALSE
    ) +
    labs(
      y        = "Medelantal per år under femårsperiod",
      title    = if (diagnos != "") {
        if_else(diagnos == "alla", "Alla utvalda diagnoser", diagnos)
        } else what,
      subtitle = subtitle,
      caption  = caption
    )


  # Add facets for diggerent age groups
  if (ages == "byage") {
    p <- p + facet_wrap(~ åldersgrupp, scales = "free")
  }

  # Save figures of specified formats
  if (!is.null(.save)) {
    paste0("graphs/tidstrender/", if (diagnos != "") diagnos else what, " - ",
           subtitle, "ages ", ages, .save) %>%
      walk(ggsave, p, height = 10, width  = 10, units  = "cm")
  } else {
    p
  }
}
