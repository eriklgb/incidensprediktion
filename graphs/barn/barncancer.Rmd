---
title: "Barncancer"
author: "Erik Bülow"
date: "20 augusti 2017"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Introduktion

I detta dokument beskrivs en enklare prediktion av incidens för barncancer i VGR.
Övrig prediktion för "En ännu bättre strålbehandling" har gjorts med en APC (Age-Period-Cohort) modell. Detta är inte möjligt för barncancer.

Vi begränsar oss här till VGR efftersom befolkningsprognos finns endast för detta område. 

Vi inkluderar samtliga fall av cancer (oberoende av diagnos) som drabbat barn och ungdomar 3-17 år.


# Antal fall relaterat till antal individer

Vi börjar med att illustrera ett eventuellt samband mellan antalet individer och antalet av dessa som rapporterats ha fått cancer:

```{r}
knitr::include_graphics("pyr_vs_fall.png")
```

Vi ser at spridningen är stor att det är svårt att urskilja något mönster. Den linjära regressionslinjen är nästan vågrät. Konfidensintervallet är relativt brett och indikerar att lutningen inte är statistiskt signifikant skild från 0. Den lilla lutning som noteras tyder på att fler individer leder till färre cancerfall. Detta känns inte som en intuitivt rimlig slutsats. Finns ett sådant samband beror det sannolikt på confounders.

Vi undersöker även om det finns ett samband med hjälp av poisson-regression, vilket det heller inte gör.


# Antal fall per år

Vi kan också illustrera utvecklingen över tid:

```{r}
knitr::include_graphics("fall_vs_ar.png")
```

Vi ser möjligtvis en svag ökning över tid men fluktuationen är stor. Befolkningsutvecklingen för denna åldersgrupp är känslig för olika stora barnkullar, vilket i sin tur kan ha påverkats av exempelvis konjunkturcykler. Vi ser att befolkningsprognosen är väldigt positiv för denna åldersgrupp. Tyvärr är det svårt att riktigt veta varför då vi saknar exaktare insikt i hur denna prognos gjorts.

Vi ser dock att antalet cancerfall per 100 000 individer nära följer trenden av absoluta antalet fall (tittar man riktigt noga kan dock vissa skillnader observeras).


# Prognos

I samband med den ovan nämnda poissonmodelleringen noterades att årtalet uppvisade en betydligt starkare association med antalet cancerfall än vad antalet individer gjorde. Vi illustrerar detta med hjälp av en linjär regressionsmodell:

```{r}
knitr::include_graphics("fall_vs_ar_pred.png")
```

Det färgade området utgör ett 95-procentigt prediktionsintervall. 

