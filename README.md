# Incidensprediktion

Med hjälp av detta projekt skapas statistiskt/epidemiologiskt underlag till "En ännu bättre strålbehandling.

# Arbete med projektet

Projektet bygger på en strukturen som ansvänds av [ProjectTemplate](http://projecttemplate.net/).

Projektet kan klonas lokalt och öppnas i RStudio. Dock saknas en datafil (med persondata). 
Denna finns tillgänglig via kansliservern `Kansli/Statistiker/erik_bulow/incidensprediktion/`
Filen `rtr.tab` är originalet men ta även `rtr.tab.rds` som är en cachad version. Kopiera båda dessa filer till den lokala Git-klonen under `data-raw`.


# Paket som behövs

I filen "config/global.dcf" anges vilka paket som behövs. De flesta av dessa kan tas (vid skrivande stund i dess senaste version) direkt från CRAN.
Det finns några undantag som måste installeras från GitHub resp Bitbucket. Kopiera följande rader till R:

```
devtools::install_github("haraldwf/nordpred")
devtools::install_bitbucket("cancercentrum/rccmisc")
devtools::install_bitbucket("cancercentrum/incadata")
devtools::install_bitbucket("cancercentrum/decoder")
```

En intruduktion till projektet finns sedan i `docs/overlamning.Rpres`.

# Tidyverse

I detta projekt används principer från Tidyverse. För den som inte är van vid detta arbetssätt rekommenderas [Hadleys kenyote från RStudio::konf](https://www.rstudio.com/resources/videos/data-science-in-the-tidyverse/).
Mer information på [Tidyverse hemsida](https://www.tidyverse.org/) och i boken [R for data science](http://r4ds.had.co.nz/).
