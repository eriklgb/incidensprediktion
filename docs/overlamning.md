En ännu bättre strålbehandling
========================================================
author: Erik Bülow
date: 2017-08-18
autosize: true



Upplägg
========================================================

- Kort intro om själva projektet
- Statistiskt/epidemiologiskt bidrag
- Tekniska aspekter - R-projekt/koder etc

En ännu bättre strålbehandling
========================================================

- Utredning av regionens framtida strålbehandlingsbehov
- Politiskt uppdrag
- RCC fått uppdraget
- Prové sköter projektledning
- Två arbetsgrupper + projektledning
- Statistiker adjungerad till arbetsgrupp 2
- Har haft ett par möten

Statistiskt underlag
========================================================

- Gruppen har önskat epidemiologiskt/statistiskt underlag för sitt arbete
- Utgår från Regionala utvecklingsplanen
- Utgår i sin tur från motsv plan från region syd

Diagnoser
=======================================================

- Från början 10 vanligaste diagnoserna
- Nyligen utökat med CNS, Gyn och Huvud-hals
- Önskemål om barncancer


Prognos av incidens
========================================================

- Age-Period-Cohort-modell (APC)
- Finns artiklar från 2002-2003
- Finns R-paket https://github.com/haraldwf/nordpred
- Utgår från befolkningsprognos
  - Nationell från SCB
  - För VGR från regionens statistikdatabas
  - Ingen för Halland. Gjort egen.
- Två olika prognoser
  - Enbart befolkningsutveckling
  - Även observerad incidenstrend


Prevalens
========================================================

- Bygger på samma principer (finns även andra metoder)
- Endast en metod (befolkning + incidenstrend)
- Punktprevalens 10 år.


SBU-rapport
========================================================

- SBU-rapport från tidigt 2000-tal hade skattningar av andel nydiagnostiserade
som fick strålbehandling
- Gäller urval av de diagnoser vi tittar på
- Finns farhåga att uppgifterna daterade
- Vi försöker dock använda detta för skattning av RT-behov


Givna behandlingar
========================================================

- Har även tittat på faktiskt antal givna behandlingar
- Extern data från Jesper/Lisa (andra medlemmar av projektet)
- Endast tillgång till aggregerad data (ej på diagnosnivå, efter ålder, eller geografisk härkomst ect)


Folkhälsodata
========================================================

- Fanns också önskemål om prediktion baserat på folkhälsodata
- Visst underlag från Folkhälsomyndigheten undersöktes
- Visade sig dock för svårt/omfattande
- Avskrevs
- RCC:s (cancerpreventionskalkylator)[http://www.cancercentrum.se/syd/vara-uppdrag/prevention-och-tidig-upptackt/prevention/cancerpreventkalkylator/] finns dock som ett enklare alternativ (bygger på internationella studier etc, ej regional data)

Geografi
========================================================

- Önskemål om VGR + Halland (hela)
- Incidens blev så
- Prevalens endast för Västra Sjukvårdsregionen
- Även uppdelat per HSN
- Diskussioner om GIS-visualisering


Ålder
========================================================

- Samtliga åldergrupper kombinerat
- Sedan även uppdelat på olika åldersgrupper


Rapport
========================================================

- RCC tagit fram underlagsrapport: https://bookdown.org/eriklgb/incidensprediktion/
- Prové plockar utvalda delar och inkorporerar i projektets slutrapport
- Detta presenteras slutligen för politiker etc


Tekniskt
========================================================

- R
- Hela projektet nu klonat till [Kansli/Statistiker/erik_bulow/incidensprediktion](Kansli/Statistiker/erik_bulow/incidensprediktion)
- Kanske även kan lägga på BitBucket?
- R-studioprojekt
- Några paket från RCC:s BitBucket (decoder, rccmisc och incadata)
- Nordpred från Github enl ovan (inte vår egen version!)
- ProjectTemplate-projekt: http://projecttemplate.net/
  - /data (med vissa referenser till /data-raw)
  - /munge - förberedande data management
  - /src - analyser och skapande av grafer ect
- Rapport skapad mha bookdown https://bookdown.org/
- R-paket 'pxweb' för inhämtning av publik data
- Mycket tidyverse: https://www.tidyverse.org/ (http://r4ds.had.co.nz/)
- Har ffa en stor nested data frame "all_data" som innehåller all relevant info
