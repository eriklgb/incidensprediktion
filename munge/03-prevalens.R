################################################################################
#                                                                              #
#                        Hjälpfunktioner för prevalens                         #
#                                                                              #
################################################################################

how_many <- function(year, years_back = 10, data) {
  prev_day <- as.Date(paste0(year, "-06-01"))
  data %>%
    filter(
      diadat <= prev_day, # Måste ha fått diagnosen före prevalensdagen
      is.na(dödat) | dödat >= prev_day, # Ska leva vid prev-dagen
      prev_day - diadat <= years_back * 365.241 # Diagnosen max years_back
    ) %>%
    distinct(pnr, .keep_all = TRUE) %>%
    mutate(ålder = suppressMessages(sweidnumbr::pin_age(pnr, prev_day))) %>%
    count(ålder)
}

# Funktion som tar ett dataset och som som för detta skapar nordpred data
prev_data <- function(x) {
  data_frame(år = 1981:2015) %>%
  mutate(pr = map(år, how_many, data = x)) %>%
  unnest(pr) %>%
  tidy2nordpred()
}


################################################################################
#                                                                              #
#                       Dataset för prevalensberäkningar                       #
#                                                                              #
################################################################################

# Vi vill ha data per diagnos i VGR
# (vi saknar persondata för S:a Halland och säker
# pyr-predektion för hela Halland)
rtr_prev_vast <-
  rtr_pop %>%
  select(diagnos, diadat, dödat, pnr) %>%
  mutate(
    region  = "Västra sjukvårdsregionen"
  )

# Vi vill ha data per HSN för samtliga diagnoser aggregerat
rtr_prev_hsn <-
  rtr_pop %>%
  transmute(
    diadat = diadat,
    dödat = dödat,
    pnr = pnr,
    diagnos = "alla",
    region  = suppressWarnings(decode(hemfr, hsn))
  ) %>%
  # Remove patients from other regions and from Halland, since we have no
  # good pyr predictions here.
  filter(!is.na(region))


################################################################################
#                                                                              #
#                       Prevalensdata i Nordpred-format                        #
#                                                                              #
################################################################################

prevs <-
  bind_rows(rtr_prev_vast, rtr_prev_hsn) %>%
  nest(-region, -diagnos, .key = nordpred_in) %>%
  mutate(nordpred_in = map(nordpred_in, prev_data))
