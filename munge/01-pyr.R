################################################################################
#                                                                              #
#                           Prepare population pyr                             #
#                                                                              #
################################################################################

# Observerad bef för hela riket. Döp om regioner till samma som för
# prognospyr för att lättare kuna kombinera
# Exkludera pyr för Halland 2016. Då vi saknar prognospyr för Halland vill vi
# skatta denna mha bef pyr. Detta görs för hela femårsperioder (inkl 2016)
bef_obs <-
  bef_observerad %>%
  mutate(
    region = suppressWarnings(
      coalesce(decode(region, hsn), gsub("s län", "", substring(region, 4)))
    )
  ) %>%
  filter(!(region == "Halland" & år == 2016))


################################################################################
#                                                                              #
#                           Combine population pyr                             #
#                                                                              #
################################################################################

pyrs <-
  suppressWarnings(
    bind_rows(
      bef_obs,
      mutate(bef_prognos_riket, region = "Riket"),
      rename(bef_prognos_vgr, år = tid)
    )
  ) %>%
  nest(-region, .key = pyr) %>%
  mutate(
    pyr = map(pyr, tidy2nordpred)
  )


################################################################################
#                                                                              #
#                  Predict Halland population based on Riket                   #
#                                                                              #
################################################################################

# Make nordpred prediction for population in Halland by using national pop as
# pyrs and observed Halland pop as outcome.
# Replace Halland pyr by new pyr including the future
pyrs$pyr[pyrs$region == "Halland"][[1]] <-
  nordpred::nordpred(
    pyrs$pyr[pyrs$region == "Halland"][[1]],
    pyrs$pyr[pyrs$region == "Riket"][[1]],
    startestage = 1,
    startuseage = 1
  ) %>%
  {round(.$predictions)}

# Add summarised pyr for the whole region
pyrs <- bind_rows(pyrs, data_frame(region = "VGR + Halland"))
pyrs$pyr[pyrs$region == "VGR + Halland"][[1]] <-
  pyrs$pyr[pyrs$region == "Halland"][[1]] +
  pyrs$pyr[pyrs$region == "Västra Götaland"][[1]]


################################################################################
#                                                                              #
#                            VGR population by year                            #
#                                                                              #
################################################################################

# We need some data to use with RT cases from Jesper/Lisa.
# We there use simple linear regression, hence don't nee the Nordpred structure

obs_data_vgr <-
  bef_observerad %>%
  filter(
    region == "14 Västra Götalands län",
    år %in% 2000:2016
  )

pred_data_vgr <-
  bef_prognos_vgr %>%
  filter(region == "Västra Götaland") %>%
  rename(år = tid)

# Observerad andel gamla i VGR
bef_rtdata_vgr <-
  bind_rows(obs_data_vgr, pred_data_vgr) %>%
  mutate(år = as.integer(as.character(år)))
bef_vgr <- count(bef_rtdata_vgr, år, wt = values)
cache("bef_rtdata_vgr")
cache("bef_vgr")
