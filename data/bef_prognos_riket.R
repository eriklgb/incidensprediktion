# Befolkningsprognos för hela riket 2016-2024
# Observera att vi här har separata åldradr ändå till 104 år!

bef_prognos_riket <-
  pxweb::get_pxweb_data(
    url = "http://api.scb.se/OV0104/v1/doris/sv/ssd/BE/BE0401/BE0401A/BefolkprognRev2017",
    dims = list(
      Alder        = c(0:104, '105+'),
      Kon          = c('*'),
      ContentsCode = c('000001IF'),
      Tid          = as.character(2017:2030)
    ),
    clean = TRUE
  )

ProjectTemplate::cache("bef_prognos_riket")
