# Prognostiserad incidens {#incres}

Resultatet av prognosen presenteras grafiskt i figur \@ref(fig:res).

Punkterna i respektive graf markerar observerade medeltalet per femårsintervall. Heldragna linjer mellan dessa punkter är dragna endast för att förbinda punkterna. De ska ej tolkas som interpolerade värden mellan mätpunkterna.

Prickade linjer markerar på samma sätt den prognostiserade utvecklingen baserad på observerad och prognostiserad befolkningsutveckling de närmaste åren. 

Streckade linjer tar hänsyn även till observerade incidenstrender som ej förklaras av förändrad befolkningsstruktur (den så kalalde "driftsfaktorn" enligt avsnitt \@ref(drift)). För perioden 2016-2020 inkluderas tidigare befolkningstrend fullt ut men för 2021-2025 enbart till 75 procent. 

Prognostiserade antal framkommer också i klartext inom respektive figur samt i tabell \@ref(tab:predtab).

```{r res, fig.align='center', fig.cap = "Observerad och prognostiserad incidens för de tio vanligaste diagnoserna i VGR och Halland."}
knitr::include_graphics("images/facets.png")
```


```{r predtab}

load("../cache/tabelldata_pred.RData")

knitr::kable(tabelldata_pred, caption = "Antalet prognostiserade fall baserat på enbart befolkningstrend (bef) samt befolkningstrend och incidenstrend (bef + inc).", booktabs = TRUE)
```


