# Prevalens

Vid arbetsmöte för grupp 2, 2017-03-31, beslutades att denna rapport utökas med prediktion av 10-prevalens. 


## Vad menar vi med 10-årsprevalens

Med *(punkt)prevalens* avser vi antalet individer som vid en viss tidpunkt lever med en cancerdiagnos. Den generella (total-)prevalensen avser således diagnoser satta vid en obestämd tidpunkt tillbaks i tiden. 

Ofta önskar man dock begränsa den tillbakablickande tidsperioden. Syftet är då att få ett mått på det antal individer som fortfarande kan antas ha ett mer aktivt vårdbehov. Detta blir relevant i de situationer man kan anta att en individ som fick sin diagnos väldigt lång tid tillbaka i tiden, inte längre påverkas av densamma. Då cancer är en "kronisk" åkomma talar man inte i termer av medecinsk "bot". Dock används vid populationsstudier begreppet "statistiskt bot", vilket är en 
Files
Plots
Packages
Help
Viewer

	
New Folder
	
	
Delete

	
Rename
	
	
More


			
Go to project directory...
Home	Documents	specialuppdrag	incidensprediktion	reports


Name
Size
Modified

..


.gitignore
72 B
Jan 18, 2017, 7:21 AM


.Rbuildignore
28 B
Feb 15, 2017, 1:26 PM


.RData
15.3 KB
Jul 21, 2017, 10:41 AM


.Rhistory
16.7 KB
Jul 28, 2017, 12:53 PM


.travis.yml
854 B
Jan 18, 2017, 7:21 AM


00-introduktion.Rmd
3.5 KB
Jun 16, 2017, 2:09 PM


01-befolkning.Rmd
11 KB
Jun 16, 2017, 2:18 PM


02-observerad_cancer.Rmd
3.2 KB
Jun 16, 2017, 2:20 PM


03-metod.Rmd
7 KB
Jul 21, 2017, 10:24 AM


03-metod.Rmd~
6.4 KB
Mar 24, 2017, 12:32 PM


04-resultat.Rmd
1.3 KB
Jun 16, 2017, 2:02 PM


040-resultat_byage.Rmd
1.7 KB
Jun 16, 2017, 2:02 PM


041-resultat_byhsn.Rmd
2.1 KB
Jun 16, 2017, 2:02 PM


04a-diskussion.Rmd
5.1 KB
Jun 9, 2017, 1:55 PM


05-folkhalsa.Rmd
6.1 KB
Mar 24, 2017, 5:47 PM


10-antal_vardade.Rmd
3.9 KB
May 5, 2017, 10:34 AM


11-prevalens.Rmd
4.2 KB
Apr 11, 2017, 6:53 AM


12-flyttmonster.Rmd
4 KB
May 5, 2017, 2:02 PM


13-prevalensmetod.Rmd
3.5 KB
May 5, 2017, 2:34 PM


14-prevalens_resultat.Rmd
1.4 KB
Jun 9, 2017, 6:34 AM


141-prevalens_diskussion.Rmd
8.6 KB
Jun 9, 2017, 3:09 PM


15-sbu.Rmd
11.3 KB
Jun 16, 2017, 12:53 PM


16-rt_gbg.Rmd
5.9 KB
May 12, 2017, 10:05 AM


17-rt_vgr.Rmd
4.8 KB
Jun 16, 2017, 10:57 AM


90-refs.Rmd
53 B
Mar 24, 2017, 11:46 AM


_book


_bookdown.yml
81 B
Feb 15, 2017, 1:55 PM


_bookdown_files


_build.sh
80 B
Jan 18, 2017, 7:21 AM


_deploy.sh
398 B
Jan 18, 2017, 7:21 AM


_output.yml
490 B
Jun 9, 2017, 2:28 PM


book.bib
20.4 KB
Feb 16, 2017, 1:49 PM


bookdown-demo.Rproj
277 B
Aug 15, 2017, 6:54 PM


cache


data


DESCRIPTION
104 B
Jan 18, 2017, 7:21 AM


images


incidensprediktion.knit.docx
15.1 MB
Jun 9, 2017, 8:31 PM


index.Rmd
4.2 KB
Jun 16, 2017, 1:58 PM


preamble.tex
161 B
Jan 18, 2017, 7:21 AM


rsconnect


style.css
172 B
Jan 18, 2017, 7:21 AM


toc.css
2.4 KB
Jan 18, 2017, 7:21 AM
	
02-observerad_cancer.Rmd

	
01-befolkning.Rmd

	
03-metod.Rmd

	
11-prevalens.Rmd

	
12-flyttmonster.Rmd

	
10-antal_vardade.Rmd

	
13-prevalensmetod.Rmd

	
14-prevalens_resultat.Rmd

	
15-sbu.Rmd

	
041-resultat_byhsn.Rmd

	
04-resultat.Rmd

	
incidensprediktion.Rmd

	
17-rt_vgr.Rmd

	
040-resultat_byage.Rmd

	
16-rt_gbg.Rmd

	
incidensprediktion.knit.md

	
00-introduktion.Rmd

	
141-prevalens_diskussion.Rmd

	
index.Rmd


			

			
	
			
	
			
	
			

			
		
	
Knit

	


			

	
Insert

		
			

			
			
	
Run

	

			

			
	
			
1:1
	
Prevalens
R Markdown

Prevalens
Vad menar vi med 10-årsprevalens
Antal fall
Mättillfällen
Vad påverkar prevalensen?
Var kan vi inhämta informationen?

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
#·Prevalens¬
¬
Vid·arbetsmöte·för·grupp·2,·2017-03-31,·beslutades·att·denna·rapport·utökas·med·prediktion·av·
10-prevalens.·¬
¬
¬
##·Vad·menar·vi·med·10-årsprevalens¬
¬
Med·*(punkt)prevalens*·avser·vi·antalet·individer·som·vid·en·viss·tidpunkt·lever·med·en·
cancerdiagnos.·Den·generella·(total-)prevalensen·avser·således·diagnoser·satta·vid·en·obestämd·
tidpunkt·tillbaks·i·tiden.·¬
¬
Ofta·önskar·man·dock·begränsa·den·tillbakablickande·tidsperioden.·Syftet·är·då·att·få·ett·mått·på·
det·antal·individer·som·fortfarande·kan·antas·ha·ett·mer·aktivt·vårdbehov.·Detta·blir·relevant·i·
de·situationer·man·kan·anta·att·en·individ·som·fick·sin·diagnos·väldigt·lång·tid·tillbaka·i·tiden,·
inte·längre·påverkas·av·densamma.·Då·cancer·är·en·"kronisk"·åkomma·talar·man·inte·i·termer·av·
medecinsk·"bot".·Dock·används·vid·populationsstudier·begreppet·"statistiskt·bot",·vilket·är·en·
modell·för·att·anta·att·den·del·av·populationen·som·överlevt·sin·diagnos·tillräckligt·länge,·kan·
(generellt·sett)·antas·sakna·en·ökad·dödsrisk·till·följd·av·sagda·diagnos.·Denna·del·av·
populationen·har·således·(generellt·sett)·samma·mortalitet·som·en·i·alla·avseende·liknande·grupp·
individer·men·utan·tidigare·cancerdiagnos.·Med·liknande·individer·menar·vi·i·Sverige·ofta·en·
ålders-·och·könsmatchad·grupp.·I·länder·där·sjukvårdstillgång·i·högre·grad·differentieras·utifrån·
socioekonomi·(såsom·Storbritannien)·vill·man·också·ofta·justera·för·detta,·och·i·länder·där·etnisk·
tillhörighet·kan·förväntas·påverka·vårdtillgången·(såsom·i·USA)·brukar·man·också·justera·för·detta·
(ofta·white/black/hispanic/others).¬
¬
Den·10-årsprevalens·vi·söker·är·alltså·det·antal·individer·som·vid·en·viss·tidpunkt·är·i·livet·och·
som·fått·en·cancerdiagnos·under·de·senaste·10·åren·före·denna·tidpunkt.·¬
¬
¬
##·Antal·fall¬
¬
Prevalens·presenteras·ofta·som·andelen·individer·från·en·population·med·en·diagnos·eller·åkomma.·
Vi·väljer·dock·att·redovsa·absolut·antal,·vilket·innebär·att·antalet·(om·allt·annat·lika)·stiger·i·
takt·med·populationen.¬
¬
¬
##·Mättillfällen¬
¬
Till·skillnad·från·incidensen·(antalet·diagnoser·satta·under·en·viss·tidsperiod),·mäts·alltså·
(punkt)prevalensen·vid·en·fix·tidpunkt.·Vi·kan·exempelvis·välja·första·juli·som·ett·representativt·
datum·för·respektive·år·(ofta·väljs·årsskiftet).¬
¬
¬
##·Vad·påverkar·prevalensen?¬
¬
Prevalensen·påverkas·dels·av·hur·många·individer·som·får·en·cancerdiagnos,·dels·av·hur·länge·dessa·
individer·lever.·¬
¬
En·hög·prevalens·kan·alltså·vara·både·"bra·och·dåligt".·Det·kan·innebära·dels·att·många·individer·
får·diagnosen·(dåligt)·men·det·kan·också·innebära·att·de·individer·som·får·diagnosen·lever·länge·
(bra).·¬
¬
Bästa·sättet·att·modellera·(och·prediktera)·prevalensen·är·alltså·att·göra·antaganden·dels·om·
incidensen·(vilket·gjorts·ovan),·dels·också·om·överlevnad/mortalitet.·¬
¬
¬
##·Var·kan·vi·inhämta·informationen?·{#flyttning}¬
¬
Incidens·mäts·via·antalet·diagnoser·registrerade·till·cancerregistret·(via·regionala·
tumörregister).·Alla·diagnoser·satta·inom·en·region·ska·finas·registrerade·i·den·regionen.·För·
prevalensen·är·det·svårare.·Vi·vill·veta·hur·många·indivder·i·vår·region·som·har·en·cancerdiagnos.·
Från·regionala·tumörregistret·saknkas·dock·information·om·de·individer·som·fått·sin·diagnos·ställd·
utanför·regionen·men·som·därefter·flyttat·hit.·¬
¬
Här·finns·dels·indivder·från·andra·regioner·i·Sverige,·dels·från·andra·länder.·För·de·förstnämnda·
finns·information·om·diagnosdatum·i·andra·regionala·tumörregister·samt·hos·Socialstyrelsen.·
Teknisk·tillgång·till·dessa·patienter·kan·ges·inom·kort·efter·att·cancerregistret·flyttat·in·på·
INCA.·Av·juridiska·skäl·kan·det·dock·ändå·vara·svårt·att·sammanställa·informationen·utan·
bilaterala·mellanregionala·kontakter.¬

Console~/Documents/specialuppdrag/incidensprediktion/reports/
Console
Terminal

~/Documents/specialuppdrag/incidensprediktion/reports/	
			

			
modell för att anta att den del av populationen som överlevt sin diagnos tillräckligt länge, kan (generellt sett) antas sakna en ökad dödsrisk till följd av sagda diagnos. Denna del av populationen har således (generellt sett) samma mortalitet som en i alla avseende liknande grupp individer men utan tidigare cancerdiagnos. Med liknande individer menar vi i Sverige ofta en ålders- och könsmatchad grupp. I länder där sjukvårdstillgång i högre grad differentieras utifrån socioekonomi (såsom Storbritannien) vill man också ofta justera för detta, och i länder där etnisk tillhörighet kan förväntas påverka vårdtillgången (såsom i USA) brukar man också justera för detta (ofta white/black/hispanic/others).

Den 10-årsprevalens vi söker är alltså det antal individer som vid en viss tidpunkt är i livet och som fått en cancerdiagnos under de senaste 10 åren före denna tidpunkt. 


## Antal fall

Prevalens presenteras ofta som andelen individer från en population med en diagnos eller åkomma. Vi väljer dock att redovsa absolut antal, vilket innebär att antalet (om allt annat lika) stiger i takt med populationen.


## Mättillfällen

Till skillnad från incidensen (antalet diagnoser satta under en viss tidsperiod), mäts alltså (punkt)prevalensen vid en fix tidpunkt. Vi kan exempelvis välja första juli som ett representativt datum för respektive år (ofta väljs årsskiftet).


## Vad påverkar prevalensen?

Prevalensen påverkas dels av hur många individer som får en cancerdiagnos, dels av hur länge dessa individer lever. 

En hög prevalens kan alltså vara både "bra och dåligt". Det kan innebära dels att många individer får diagnosen (dåligt) men det kan också innebära att de individer som får diagnosen lever länge (bra). 

Bästa sättet att modellera (och prediktera) prevalensen är alltså att göra antaganden dels om incidensen (vilket gjorts ovan), dels också om överlevnad/mortalitet. 


## Var kan vi inhämta informationen? {#flyttning}

Incidens mäts via antalet diagnoser registrerade till cancerregistret (via regionala tumörregister). Alla diagnoser satta inom en region ska finas registrerade i den regionen. För prevalensen är det svårare. Vi vill veta hur många indivder i vår region som har en cancerdiagnos. Från regionala tumörregistret saknkas dock information om de individer som fått sin diagnos ställd utanför regionen men som därefter flyttat hit. 

Här finns dels indivder från andra regioner i Sverige, dels från andra länder. För de förstnämnda finns information om diagnosdatum i andra regionala tumörregister samt hos Socialstyrelsen. Teknisk tillgång till dessa patienter kan ges inom kort efter att cancerregistret flyttat in på INCA. Av juridiska skäl kan det dock ändå vara svårt att sammanställa informationen utan bilaterala mellanregionala kontakter.

Patienter diagnossatta i andra länder är vi dessvärre nödgade att helt exkludera. Om de fått vård  efter inflytt till regionen skulle det visserligen vara teoretiskt möjligt att inhämta information om diagnos via patientregister (se avsnitt \@ref(npr)). Där saknas dock information om diagnosdatum. Endast totalprevalensen skulle därmed vara (teoretiskt) möjlig att identifiera på detta sätt. Till denna rapport görs ingen ansats att fånga prevalens bland inflyttande från annat land. 
