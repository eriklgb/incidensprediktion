## Resultat per åldersgrupp {#incbyage}

Vi önskar här också presentera samma resultat som i avsnitt \@ref(resultat) men nu uppdelat på olika åldersgrupper.

För enkelhets skull väljer vi att använda samma åldersuppdelning för samtliga diagnoser. Detta även om åldersfördelningen inom respektive diagnos kan variera. Exakt hur vi väljer att göra denna åldersuppdelning är arbiträrt och kan justeras vid behov!

Antal observerade fall per diagnos och åldersgrupp redovisas i tabell \@ref(tab:top10byage).


```{r top10byage}
load("../cache/tabelldata_byage.RData")
tabelldata_byage %>% 
  knitr::kable(caption = "Geomsnittligt antal diagnostiserade fall per femårsperiod och åldersgrupp för de tio vanligaste diagnoserna i VGR och Halland.", booktabs = TRUE, longtable = TRUE)
```

Antalet predicerade fall redovisas på samma sätt i tabell \@ref(tab:predtabbyage).


```{r predtabbyage}
load("../cache/tabelldata_pred_byage.RData")
knitr::kable(tabelldata_pred_byage, caption = "Antalet prognostiserade fall baserat på enbart befolkningstrend (bef) samt befolkningstrend och incidenstrend (bef + inc).", booktabs = TRUE, longtable = TRUE)
```

Samma resultat redovisas även grafiskt i figur \@ref(fig:resbyage).
Bilden är ganska stor kan men kan eventuellt förminskas as webbläsaren för att passa in på sidan. Det bör dock gå att antingen zooma in eller att spara ner bilden separat och sedan öppna i ett bildvisningsprogram som kan visa den i full skala. 

```{r resbyage, fig.align='center', fig.cap = "Observerad och prognostiserad incidens för de tio vanligaste diagnoserna i VGR och Halland."}
knitr::include_graphics("images/facets_byage.png")
```

