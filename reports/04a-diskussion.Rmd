# Diskussion {#Diskussion}

Vi har prognostisera antalet diagnostiserade cancerfall för kommande tre femårsperioderer. Vi har använt två olika driftstrender och ser i figur \@ref(fig:res) att dessa båda skattningar ibland skiljer sig, ibland inte. Låt oss titta lite närmare på respektive diagnos.


## Bröstcancer

För bröstcancer ses historiskt en ökning i incidensen utöver vad som kan förklaras enbart på grund av befolkningens tillväxt och åldrande. Fortsätter denna ökning i samma takt kan vi således förvänta oss en kraftigare ökning än om vi tror att denna ökningen varit av mer tillfällig art. Om ökad diagnostisering exempelvis beror på ökad screening så kan det vara en övergående effekt (man "betar av" ett antal fall som annars skulle diagnostiseras först vid ett senare tillfälle). 

Från och med 1 juli 2016 infördes gratis mammografiscreening i hela Sverige. Effekten av detta har inte beaktats explicit i modellen och kan därmed få effekter som ej förutses av densamma. Syftet med den nya lagstiftningen är dock att öka andelen kvinnor som screenas. En ökning av diagnostiserade bröstcancerfall torde därmed bli en trolig effekt.


## Lunga, luftstrupe och bronker

Här är skillnaden mellan de båda skattningarna relativt liten. Då vi saknar osäkerhetsmått för våra skattningar bör vi vara något försiktiga med att tolka skillnader som eventuellt inte är större än att de kan förklaras av mer slumpmässiga faktorer. 


## Magsäck

Incidensen av magcancer har minskat sedan 1960-talet, delvis till följd av ändrade kostvanor. Risken att diagnostiseras med denna typ av cancer har minskat så pass mycket att också antalen sjunkit trots en ökande folkmängd. 

Här, till skillnad från för bröst- och lungcancer ovan, ser vi att skattningen baserad på både befolkning och incidens ligger under skattningen baserad på enbart befolkningsförändring. Detta innebär att om den minskade incidenstrenden skulle upphöra så skulle vi observera en ökning till följd av fler och äldre individer. Om vi istället tror att trenden håller i sig även fortsatt, så kan vi hoppas på att denna minskning väger upp befolkningsökningen. Vi ser inte längre en minskande trend totalt men effekterna tar åtminstone ut varandra och vi tycks kunna vänta oss en mer stabil nivå framåt.


## Malignt melanom i huden

Här ser vi liksom för bröstcancer en större ökning än vad enbart befolkningsstrukturen ger upphov till. Detta förklaras ofta med ett förändrat beteendemönster avseende ökad solning, vilket får till följd att fler utsätts för riskfaktorer såsom ultraviolett strålning och därmed riskerar utveckla hudcancer. 


## Ospecificerad lokalisation

Detta är något av en slaskgrupp, varför förändringar här torde kunna tillskrivas förändrad kodsättning respektive förbättrad diagnostik snarare än någon egentlig förändring i antalet fall.

Den kraftiga nedgången i början av 2000-talet kan eventuellt förklaras av påbörjat införande av vårdprogram och kvalitetsregister och därmed ett förbättrat underlag för kodsättning till cancerregistret. 


## Prostata {#prostata}

För prostatacancer ser vi en klassisk "puckel-effekt" efter införande av PSA-testning. Effekten innebär att ett "screeningliknande" förfaringssätt inledningsvis ökar diagnostiseringen men att denna avtar då fallen som annars skulle diagnostiserats senare betats av. 

En prognos som ignorerar minskningen till följd av PSA-testning (befolkningstrenden), fortsätter peka uppåt i samma riktning som innan PSA-testningen infördes. Tas däremot hänsyn till effekten ser vi istället en fortsatt minskning som dock avtar med tid (till följd av vårt val av driftstrend). 

Vi kan här notera att vårt val av driftstrend får stor betydelse för prognosen. Härvid blir det extra önskvärt att basera prognosen inte bara på historiskt observerad data utan även på klinisk och organisatorisk kunskap därutöver. 


## Tjocktarm

Liksom för lungcancer ovan så är skillnaden mellan de två skattningsmetoderna här ganska små. 


## Tumör i huden, ej malignt melanom

Mönstret går här igen från maligna melanom. 


## Urinvägar utom njure

Här ser vi att resultaten för de båda skattningsmetoderna sammanfaller. Vi kan alltså inte observera någon riskförändring för urinvägscancer utöver vad som är att direkt förvänta utav befolkningens storlek och åldersfördelning. Detta ses också genom den nästintill räta linjen av observerade värden (att linjen dessutom följer nästan exakt 45 grader är däremot en grafisk artefakt av att axlarna tillåts variera).


## Ändtarm och anus

Även här ligger skattningarna väldigt nära varandra.


## Effekten av standardiserade vårdförlopp

Avslutningsvis kan konstateras att presenterade prognoser alltså endast utgår från observerad cancer- och befolkningsdata 1981-2015 samt prognostiserad befolkningsdata 2016-2025.

Effekter av nyligen införda standardiserade vårdförlopp har ej beaktas. Vi har dock anledning anta att effekter av SVF inte i någon större utsträckning kommer påverka just antalet diagnossatts tumörer.


