# Diskussion kring prevalens


Att tolka prevalenstalen är något mer komplext än motsvarande uppgifter för incidens (avsnitt \@ref(Diskussion)) då prevalensen beror dels på incidens, dels på eventuella förändringar i överlevnadstid efter diagnos.

Vi gör ändå ett försök att översiktligt kommentera några av resultaten observerade i avsnitt \@ref(prevres). 

Nedan görs en del implicita referenser till Socialstyrelsens och Cancerfondens rapport [Cancer i siffror](https://res.cloudinary.com/cancerfonden/image/upload/v1422262211/documents/cancer-i-siffror.pdf) från 2013.


## Övergripande observationer

Gemensamt för de flesta diagnoser är att prevalensen tycks fortsätta öka även framöver. 

För ett par diagnoser (bröst, maligna melanom, prostata samt övriga tumörer i huden) tycks dock ökningstakten avta något med tiden. Gemensamt för dessa diagnoser är att den relativa tioöverlevnaden, och därmed mortaliteten, utvecklats positiv över tid, och nu ligger på cirka 90 procent relativt en ålders- och könsmatchad populationskohort. Vi kan för dessa diagnoser anta att den relativa överlevnaden inte kan fortsätta förbättras oförändrat i samma takt. Sannolikt finns en övre gräns vid 100 procent (motsatsen skulle kunna indikera  överdiagnostik). Att ökningstakten därför bromsas in blir därmed naturligt. 

Även tjocktarm samt ändtarm och anus uppvisar liknande mönster men "avmattningseffekten" är eventuellt något svagare, vilket på samma sätt kan förklaras med att den relativa överlevnaden för dessa diagnoser ännu har en bit kvar till en potentiell taknivå.



## Bröstcancer

Vi såg av incidensprognosen att antalet diagnostiserade bröstcancerfall kan förväntas öka. För den grupp som diagnostiseras har man under en längre tid sett en ökad relativ 5- och 10-årsöverlevnad samt därmed en minskad mortalitet för denna grupp. Förändringen över tid har varit tämligen linjär, vilket här också avspeglas i en prognostiserad fortsatt linjär ökning av antalet individer i  regionen med bröstcancer diagnostiserad under de senaste tio åren.


## Lunga, luftstrupe och bronker

Liksom för incidensen ser vi även för prevalensen en något avvikande trend för lungcancer. Vi ses initialt en ökning i antalet fall men därefter ett avtagande. 
Här bör noteras att en svårighet vid prognos av just lungcancer är att vi över lång tid sett en ökande incidenstrend för kvinnor men en minskande för män. Sammanvägs dessa båda trender riskerar de cancelera varandra. Att stratifiera prognosen per kön skulle eventuellt kunna vara relevant. 

Vad gäller överlevnaden för de patienter som diagnostiserats med lungcancer har man eventuellt kunna skönja en viss positiv utveckling för både män och kvinnor över tid. Diagnosen är dock fortfarande en av de cancerdiagnoser som har sämst överlevnadsprognos. En stor del av de patienter som diagnostiseras kommer därför att bidra till prevalensen endast under en kortare period efter sin diagnos. Därmed blir prevalenstrenden mindre "trögrörlig" än om antalet prevalenta fall hade ackumulerats över längre tid. 

På grund av diagnosens höga dödlighet tenderar alltså incidensutvecklingen få en ganska direkt påverkan på antalet prevalenta fall. Då incidensen till stor del följer av rökning kan viss ljusning ses i och med att en lägre andel av befolkningen uppges vara dagligrökare över tid enligt [Folkhälsomyndigheten](https://www.folkhalsomyndigheten.se/folkhalsorapportering-statistik/folkhalsans-utveckling/levnadsvanor/tobaksrokning-daglig/). Att också färre yngre än i tidigare generationer röker kan bidra till den framöver potentiellt avtagande trenden.


## Magsäck

För magsäckscancer ses en förväntad ökning av antalet prevalenta fall. Diagnosen är mycket ovanlig bland individer yngre än 50 år. Med en högre andel äldre i befolkningen kan vi därmed förvänta oss en viss ökning av antalet incidensfall (vilket illustrerades i avsnitt \@ref(Diskussion)). 

Den kraftiga ökningen av prevalenta fall torde dock inte förklaras helt av incidensökningen utan också av förbättrad överlevnad. Enligt [Cancer i siffror](https://res.cloudinary.com/cancerfonden/image/upload/v1422262211/documents/cancer-i-siffror.pdf) gäller att:

> Att operera bort hela eller delar av magsäcken
var fram till för något år sedan [räknat från 2013] den enda
behandling som kunde ges för att bota patienten.
I dag kompletteras operationen med såväl strålbehandling
som cytostatika, som ges antingen
före eller efter operationen. Ibland ges enbart
cytostatika utöver den kirurgiska behandlingen.
Behandlingsstrategin har visat sig öka chansen
till långtidsöverlevnad hos patienterna. [...] 
[D]ödligheten i magsäckscancer [har] minskat
avsevärt. 5-årsöverlevnad efter operation med
tillägg av strålbehandling och/eller cytostatikabehandling
är cirka 50 procent. Totalt sett är dock 5-årsöverlevnaden i magsäckscancer inte
mer än drygt 20 procent. 



## Malignt melanom i huden {#mmprevdisk}

Även för maligna melanom tycks vi kunna förvänta oss en fortsatt ökning av tioårsprevalensen.  Enligt [cancer i siffror](https://res.cloudinary.com/cancerfonden/image/upload/v1422262211/documents/cancer-i-siffror.pdf) kunde en förbättrad ökning i den relativa överlevnaden noteras till och med cirka år 2000. Därefter har nivån legat på en oförändrat hög nivå på cirka 90 procent enligt [regional registerrapport 2014](http://cancercentrum.se/globalassets/cancerdiagnoser/hud/vast/regional-registerrapport-melanom-2014.pdf). I och med att vi i allmänhet lever längre så kommer därmed även patienter med maligna melanom att göra detsamma. Detta, tillsammans med den sedan tidigare observerade incidensökningen, leder till att även tioårsprevalensen kan förväntas öka över tid.


## Ospecificerad lokalisation

Prevalensen för ospecificerade fall tycks också öka över tid. Detta skiljer sig något från incidensutvecklingen, vilken noterades vara något mer volatil (notera emellertid grafernas skalskillnader vid eventuell jämförelse). Detta kan tyda på att tioårsöverlevnaden för dessa diagnoser är relativt god, varför fall ackumuleras över tid och prevalenstrenden blir "trögrörlig" även då incidenstrenden växlar. 

I övrigt är det svårt att uttala sig om just denna grupp då vi saknar djupare kunskap om dessa tumörers ursprung.


## Prostata

Vi noterade att antalet diagnostiserade prostatacancerfall kan förväntas plana ut ifall den positiva incidensutvecklingen kvarstår. Här bör dock  eventuella effekter av ökad PSA-testning vägas in. 

Prevalensen fortsätter dock öka över de närmaste tre femårsperioderna men ökningstakten minskas med tiden. Sker detta trots en eventuellt stabil incidensnivå bör förklaringen ligga i förbättrad överlevnad. En sådan trend har noterats men här bör också påtalas att ett tidigarelagt diagnosdatum (baserat på tidig upptäckt via PSA-testning) med automatik leder till bättre överlevnad. Detta eftersom överlevnadstiden börjar räknas från ett tidigare datum, varpå den uppmätta överlevnadsperioden blir längre, även i det fall dess slutpunkt lämnas opåverkad.


## Tjocktarm

Tjocktarmscancer är vanligare i den äldre delen av befolkningen. Med ett ökat antal äldre får vi, allt annat lika, fler incidenta fall. Även överlevnaden har förbättrats något över tid, vilket ytterligare bidrar till ökad prevalens. 



## Tumör i huden, ej malignt melanom

Utvecklingen för övriga hudtumörer följer samma mönster som maligna melanom, se avsnitt \@ref(mmprevdisk).


## Urinvägar utom njure

Liksom för lungcancer uppvisar urinvägscancer en initialt ökande men därefter avtagande trend. Liksom för lungcancer har vi också rätt stor skillnad i förekomst beroende på kön. Här har vi emellertid ingen "interaktionseffekt" mellan kön och incidens utan det är bara nivåerna som skiljer sig. Cirka hälften av urinvägscancerfallen tros generellt kunna förklaras av tobaksrökning. Att tobaksrökningen minskar över tid och är ovanligare bland yngre torde vara gynnsamt på längre sikt.



## Ändtarm och anus

Den relativa incidensen (antalet diagnostiserade individer per 100 000 levnadsår) har legat tämligen konstant över tid. Ökningen av diagnostiserade fall förklaras därmed av en större och åldrande befolkning. 

Även den relativa överlevnaden, och därmed mortaliteten, har legat relativt konstant under 2000-talet (efter att dessförinnan ha förbättrats över tid). Sker inget dramatiskt utveckling torde vi därmed kunna förvänta oss en ganska linjär fortsatt ökning även av antalet prevalenta fall. 

